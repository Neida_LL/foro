import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UsuarioI } from 'src/app/interface/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  noexiste:boolean =false;
  form!:UntypedFormGroup
  loading:boolean = false;
  constructor(private fb:UntypedFormBuilder, 
              private _snackbar: MatSnackBar, 
              private router:Router,
              private _serviceUser:UsuarioService  ) { 
  this.formulario();
  }

  ngOnInit(): void {
  }

  formulario():void{
    this.form=this.fb.group({
      usuario:['',Validators.required],
      password:['',Validators.required]
    })
  }

  fakeLoading():void{
    this.loading = true;
    setTimeout(() => {
      this.loading=false;
    //Redireccionamos al dashboard 
    }, 1500);
  }

  ingresar(){
    // console.log(this.form.value);
    // const Usuario:UsuarioI ={
    //   usuario: this.form.value.usuario,
    //   password: this.form.value.password,
    //   ultima_coneccion:'',
    //   cant_like:0,
    //   cant_dislike:0,
    //   lista_public:[]
    // }     
    // console.log(Usuario);

    // if (true) {
      //Redireccionamos al dashboard
      let user =this._serviceUser.getUserPass(this.form.value.usuario ,this.form.value.password);
      if (Object.entries(user).length !== 0) {
        
        this.fakeLoading();
        this._serviceUser.setUsuarioGlobal(user.usuario);
        this.router.navigate(['inicio/user/',user.usuario]);
      }else{
        this.form.reset();
        this.noexiste = true;
        setTimeout(() => {
          //Redireccionamos al dashboard 
          this.noexiste=false;
        }, 3000);
      }
  
  }


  error():void{
    this._snackbar.open('usuario o contraseña incorrecto','',{
      duration:5000,
      horizontalPosition:'center',
      verticalPosition:'bottom'
    })
  }

}
