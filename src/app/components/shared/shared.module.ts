import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

//Angular Material:
import { MatSliderModule } from "@angular/material/slider";
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatDividerModule} from '@angular/material/divider';

import { NgxEditorModule } from 'ngx-editor';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSliderModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatSnackBarModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatIconModule,
  HttpClientModule,
  MatTableModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatSortModule,
  MatCardModule,
  MatGridListModule,
  MatSelectModule,
  ReactiveFormsModule,
  FormsModule,
  MatDividerModule,
  NgxEditorModule.forRoot({
    locals: {
       // menu
       bold: 'Negrita',
       italic: 'Cursiva',
       code: 'Código',
       blockquote: 'Bloque',
       underline: 'linea',
       strike: 'Raya',
       bullet_list: 'Lista',
       ordered_list: 'Lista ordenada',
       heading: 'Párrafo',
       h1: 'Párrafo 1',
       h2: 'Párrafo 2',
       h3: 'Párrafo 3',
       h4: 'Párrafo 4',
       h5: 'Párrafo 5',
       h6: 'Párrafo 6',
       align_left: 'Izquierda',
       align_center: 'Centro',
       align_right: 'Derecha',
       align_justify: 'Párrafo',
       text_color: 'Color',
       background_color: 'Color Fondo',
      // popups, forms, others...
       url: 'URL',
       text: 'Texto',
       openInNewTab: 'Open in new tab',
       insert: 'Insert',
       altText: 'Alt Text',
       title: 'Title',
       remove: 'Remove',
       },
  }),
  
  ],
  exports:[
    MatSliderModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatSnackBarModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatIconModule,
  HttpClientModule,
  MatTableModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatSortModule,
  MatCardModule,
  MatGridListModule,
  MatSelectModule,
  ReactiveFormsModule,
  NgxEditorModule,
  FormsModule,
  MatDividerModule  
  ]
})
export class SharedModule { }
