import { Injectable } from '@angular/core';
import { PublicI, PublicIU } from '../interface/usuario.interface';
import { UsuarioService } from './usuario.service';

@Injectable({
  providedIn: 'root'
})
export class PublicacionService {

  listaPublicaciones:PublicI[] =[
    // {id:'1',usuario: 'jperez', fecha: '2022-06-28T19:30',titulo:'Cafe Teñido',contenido:'<style> h3 { color: purple; text-align:center; } </style><h3> Hello there </h3>',cant_like:1,cant_dislike:2,lista_comentarios:[]},
    // {id:'2',usuario: 'mgomez', fecha: '2022-06-28T19:30',titulo:'Expresso--',contenido:'<b>Hola</b>',cant_like:1,cant_dislike:2,lista_comentarios:[]},
    // {id:'3',usuario: 'ngarcia',fecha: '2022-06-28T19:30',titulo:'Todo_supeeeerr',contenido:'<b>Hola</b>',cant_like:1,cant_dislike:2,lista_comentarios:[]}
  ]
  constructor(private serviceUser:UsuarioService) { 
    this.cargarDatos();
  }

  cargarDatos(){
    //-------------------Publicaciones-----------------------------
    if (!localStorage.getItem("listPublic")) {
      this.obtenerDatosDeUsers();
    } else{
      //localStorage.setItem("listPublic", JSON.stringify( this.listaPublicaciones));
      let guardados = localStorage.getItem('listPublic');
      this.listaPublicaciones = JSON.parse(guardados || '{}');

    }
  }

  cargarDatosDeLocal(){
    //carga el local con datos y vuelve a treaerlos
    localStorage.setItem("listPublic", JSON.stringify( this.listaPublicaciones));
    let guardados = localStorage.getItem('listPublic');
      this.listaPublicaciones = JSON.parse(guardados || '{}');
  }

  obtenerDatosDeUsers(){
    // this.serviceUser.cargarDatos();
    console.log(this.serviceUser.getListUsers());
    
    let infoUsers = this.serviceUser.getListUsers();
    this.listaPublicaciones = [];
    infoUsers.forEach(user =>{
      if (user.lista_public.length > 0) {
        this.listaPublicaciones = this.listaPublicaciones.concat(user.lista_public);
        console.log(this.listaPublicaciones);
        
      }
    }); 
    this.cargarDatosDeLocal();
    // this.cargarDatos();
  } 

  getListPublic(): PublicI[] {
    this.cargarDatos();
    //slice retorna  una copia del array
    return this.listaPublicaciones.slice();
  }

  buscarPublic(id: any): PublicI{
    //o retorna un json {} vacio
    this.cargarDatos();
    return this.listaPublicaciones.find(element => element.id == id) || {} as PublicI;
  }

  agregarComent(nuevoC:PublicI){
    
    // this.cargarDatos();
    this.serviceUser.agregarPostPorComent(nuevoC);
    this.obtenerDatosDeUsers();
  }

  eliminarListUsers(id:string){
    this.listaPublicaciones =this.listaPublicaciones.filter(data => {
      return data.usuario!==id; 
    })
  }

  agregarUserList(usuario:PublicI){
    this.listaPublicaciones.push(usuario);
    localStorage.setItem("listPublic", JSON.stringify( this.listaPublicaciones));
  }
}
